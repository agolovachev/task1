package ru.alfabattle.config.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.alfabattle.model.AtmsResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "server", url = "${api.atm.check.url}")
public interface IATMServiceHttpClient {

    @RequestMapping(method = RequestMethod.GET, value = "/atms", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    AtmsResponse get(@RequestHeader("x-ibm-client-id") String clientId);
}
