package ru.alfabattle.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class JacksonBigDecimalSerializer extends JsonSerializer<BigDecimal> {

    public static final int DEFAULT_SCALE = 8;
    public static final RoundingMode DEFAULT_ROUNDING_METHOD = RoundingMode.HALF_UP;

    protected int getDefaultScale() {
        return DEFAULT_SCALE;
    }

    protected RoundingMode getDefaultRoundingMode() {
        return DEFAULT_ROUNDING_METHOD;
    }

    /**
     * Helper to provide consistent scale/{@link RoundingMode} uses scale 8 and
     * {@link RoundingMode#HALF_UP} by default and calls
     * {@link #scaleAndRound(BigDecimal, int, RoundingMode)}
     *
     * @param value
     * @return
     */
    protected BigDecimal scaleAndRound(BigDecimal value) {
        return scaleAndRound(value, getDefaultScale(), getDefaultRoundingMode()).stripTrailingZeros();
    }

    /**
     * Helper to automate scale/rounding. If rate is <code>null</code> returns
     * <code>null</code>
     *
     * @param value
     * @param scale
     * @param mode
     * @return
     */
    protected BigDecimal scaleAndRound(BigDecimal value, int scale, RoundingMode mode) {
        return (value == null) ? null : value.setScale(scale, mode);
    }

    @Override
    public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeNumber(scaleAndRound(value));
    }
}
