package ru.alfabattle.model;

import lombok.Data;

import java.util.List;

@Data
public class AtmsResponseData {

    private List<ATM> atms;
}
