package ru.alfabattle.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Coordinates {

    private BigDecimal latitude;
    private BigDecimal longitude;
}
