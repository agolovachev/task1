package ru.alfabattle.model;

import lombok.Data;

@Data
public class ATM {

    private Address address;
    private Coordinates coordinates;
    private Services services;
    private Integer deviceId;
}
