package ru.alfabattle.model;

import lombok.Data;

@Data
public class AtmsResponse {

    private AtmsResponseData data;
}
