package ru.alfabattle.model;

import lombok.Data;

@Data
public class Address {

    private String city;
    private String location;
    private String mode;
}
