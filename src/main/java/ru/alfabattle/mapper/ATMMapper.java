package ru.alfabattle.mapper;

import org.mapstruct.Mapper;
import ru.alfabattle.model.ATM;
import ru.alfabattle.model.ATMRo;

@Mapper
public interface ATMMapper {

    ATMRo transform(ATM atm);
}
