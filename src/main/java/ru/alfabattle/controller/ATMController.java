package ru.alfabattle.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.alfabattle.exception.BadRequestDataException;
import ru.alfabattle.mapper.ATMMapper;
import ru.alfabattle.model.ATMRo;
import ru.alfabattle.service.IATMService;

import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping(path = "/atms")
public class ATMController {

    private final IATMService atmService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<ATMRo> getById(@PathVariable(required = false) Integer id) {

        if (id == null) {
            throw new BadRequestDataException("atm not found");
        }

        ATMRo atm = atmService.find(id);
        if (atm == null) {
            throw new BadRequestDataException("atm not found");
        }

        return ResponseEntity.ok(atm);
    }
}

