package ru.alfabattle.service;

import ru.alfabattle.model.ATMRo;

public interface IATMService {

    ATMRo find(Integer id);
}
