package ru.alfabattle.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.alfabattle.mapper.ATMMapper;
import ru.alfabattle.model.ATM;
import ru.alfabattle.model.ATMRo;
import ru.alfabattle.config.http.IATMServiceHttpClient;
import ru.alfabattle.model.AtmsResponse;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ATMService implements IATMService {

    @NonNull
    private final ATMMapper mapper;

    @NonNull
    private final IATMServiceHttpClient httpClient;

    @Value("${api.atm.id")
    private String clientId;

    @Override
    public ATMRo find(Integer id) {

        AtmsResponse response = httpClient.get(clientId);
        if (response == null || response.getData() == null || response.getData().getAtms() == null) {
            return null;
        }

        Optional<ATM> atmOpt = response.getData().getAtms()
                .stream()
                .filter(atm -> atm.getDeviceId().equals(id))
                .findFirst();

        return atmOpt.isPresent() ? mapper.transform(atmOpt.get()) : null;
    }
}
