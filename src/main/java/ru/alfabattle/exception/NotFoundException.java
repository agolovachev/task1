package ru.alfabattle.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 7429320898159983051L;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Exception ex) {
        super(message, ex);
    }
}

