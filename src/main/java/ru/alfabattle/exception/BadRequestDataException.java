package ru.alfabattle.exception;

public class BadRequestDataException extends RuntimeException {

    private static final long serialVersionUID = 7429320898159983051L;

    public BadRequestDataException(String message) {
        super(message);
    }

    public BadRequestDataException(String message, Exception ex) {
        super(message, ex);
    }
}

