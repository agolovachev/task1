FROM openjdk:11
MAINTAINER A.Golovachev

ADD ./target/application.jar /app/
CMD ["java", "-jar", "/app/application.jar"]
